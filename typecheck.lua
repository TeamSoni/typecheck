--[[
Typecheck
v1.0
--]]

local apow = require "apow"
local tunpack = unpack or table.unpack
local tconcat = table.concat
local cocreate = coroutine.create
local corunning = coroutine.running
local coyield = coroutine.yield
local coresume = coroutine.resume
local costatus = coroutine.status
local error = error
local type = type
local setmetatable = setmetatable
local ipairs = ipairs

local dummy = {}
local ischecked = {}
setmetatable(ischecked, {__mode="k"})

local checked = apow.annotation ^ function(self, target)
  return function(...)
    local co = cocreate(target)
    local function recurse2(recurse, ok, ...)
      ischecked[co] = nil
      local status = costatus(co)
      if status == "dead" then
        if ok then
          return ...
        else
          error((...))
        end
      else
        if ok then
          if ... == dummy then
            local _, err = ...
            return nil, err
          end
          return recurse(coyield(...))
        else
          -- TODO handle this error condition
          error("Unexpected error condition")
        end
      end
    end
    local function recurse(...)
      if costatus(co) == "dead" then
        -- TODO handle this error condition
        error("Unexpected error condition")
      end
      ischecked[co] = true
      return recurse2(recurse, coresume(co, ...))
    end
    return recurse(...)
  end
end

-- can't use apow for these
local check = {}
local uncheck = {}

local checkmt = {}
local uncheckmt = {}

checkmt.__pow = function(self, target)
  if not ischecked[corunning()] then error("Attempt checked typecheck in unchecked context") end
  local t = {}
  for i, v in ipairs(self) do
    local fun, value = tunpack(v, 1, 3)
    if fun(target) == value then
      return target
    else
      t[i] = value
    end
  end
  error(coyield(dummy, tconcat(t, ", ") .. " expected, got " .. type(target)))
end
checkmt.__call = function(self, target, value)
  if type(target) == "string" then
    target, value = type, target
  end
  local x = {target, value}
  local newself
  if self == check then
    newself = {x}
  else
    newself = {x, tunpack(self)}
  end
  setmetatable(newself, checkmt)
  return newself
end
setmetatable(check, checkmt)

uncheckmt.__pow = function(self, target)
  local t = {}
  for i, v in ipairs(self) do
    local fun, value = tunpack(v, 1, 3)
    if fun(target) == value then
      return target
    else
      t[i] = value
    end
  end
  error(tconcat(t, ", ") .. " expected, got " .. type(target))
end
uncheckmt.__call = function(self, target, value, msg)
  if type(target) == "string" then
    target, value = type, target
  end
  local x = {target, value}
  local newself
  if self == uncheck then
    newself = {x}
  else
    newself = {x, tunpack(self)}
  end
  setmetatable(newself, uncheckmt)
  return newself
end
setmetatable(uncheck, uncheckmt)

return {check = check, uncheck = uncheck, checked = checked}